var Client = require('origami-client');

function setupTokenProvider (tokenProvider, socket, apis, dependenciesSocket, dependenciesMap) {
  socket
  .on(
    'get-methods',
    function (callback) {
      callback(null, tokenProvider.describeMethods());
    }
  );
  
  socket
  .on(
    'get-token',
    function (stackToken, context, methodName, params, callback) {
      if (dependenciesMap && Object.keys(dependenciesMap).length > 0) {
        var client = new Client(dependenciesSocket, apis, stackToken);
         
        for (var localName in dependenciesMap) {
          context[localName] = client[dependenciesMap[localName]];
        }
      }
      
      tokenProvider
      .invokeMethod(
        stackToken,
        context,
        methodName,
        params
      )
      .then(function (result) {
        callback(null, result);
      })
      .catch(function (err) {
        callback(err);
      });
    }
  );
}

function TokenProviderInitializer(tokenProvider, dependenciesMap) {
  // Token Provider Initializer
  
  if (!tokenProvider) throw new Error('token provider is required');
  
  return function (socket, namespace, crane) {
    // Token Provider Initializer
  
    if (!socket) throw new Error('socket is required');
    if (!crane) throw new Error('crane is required');
    if (namespace !== 'TokenProvider.' + tokenProvider.getName()) throw new Error('namespace must be equal to TokenProvider. + plugin name');
    
    return new Promise(function (resolve, reject) {
      if (dependenciesMap && Object.keys(dependenciesMap).length > 0) {
        crane
        .createSocket(
          '',
          function (dependenciesSocket, namespace, crane) {
            dependenciesSocket
            .emit('describe-apis', function (err, apis) {
              if (err) return reject();
              
              setupTokenProvider(
                tokenProvider,
                socket,
                apis,
                dependenciesSocket,
                dependenciesMap
              );
              
              resolve(function () {
                // ready function
              });
            });
          }
        );
      } else {
        setupTokenProvider(tokenProvider, socket, {}, null, dependenciesMap);
        
        return resolve(function () {
          // ready function
        });
      }
    });
  };
}

module.exports = TokenProviderInitializer;