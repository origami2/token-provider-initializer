var assert = require('assert');
var TokenProviderInitializer = require('..');
var EventEmitter = require('events').EventEmitter;
var Plugin = require('origami-plugin');

function StubTokenProvider() {
}

StubTokenProvider.prototype.withCredentials = function(username, password) {
  if (username === 'user1' && password === 'pass1') {
    return Promise.resolve({
      username: 'user1'
    });
  } else {
    return Promise.reject('invalid credentials');
  }
};

describe('TokenProviderInitializer', function () {
  it('requires a plugin', function () {
    assert.throws(
      function () {
        new TokenProviderInitializer();
      },
      /token provider is required/
    );
  });
  
  var tokenProvider1;
  
  before(function () {
    tokenProvider1 = new Plugin(StubTokenProvider);
  });
  
  it('returns a function', function () {
    var target = new TokenProviderInitializer(tokenProvider1);
    
    assert.equal('function', typeof(target));
  });
  
  it('requires a socket', function () {
    var target = new TokenProviderInitializer(tokenProvider1);
    
    assert
    .throws(
      function () {
        target();
      },
      /socket is required/
    );
  });
  
  it('requires a socket', function () {
    var target = new TokenProviderInitializer(tokenProvider1);
    
    assert
    .throws(
      function () {
        target();
      },
      /socket is required/
    );
  });
  
  it('requires a crane', function () {
    var target = new TokenProviderInitializer(tokenProvider1);
    var fakeSocket = new EventEmitter();
    
    assert
    .throws(
      function () {
        target(
          fakeSocket,
          'Plugin.StubPlugin'
        );
      },
      /crane is required/
    );
  });
  
  describe('#get-token event', function () {
    it('invokes method', function (done) {
      var target = new TokenProviderInitializer({
        getName: function () { return 'StubTokenProvider'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          try {
            assert.deepEqual(
              {
                fakeToken: true
              },
              stackToken
            );
            assert.deepEqual(
              {
                someContext: 'someValue'
              },
              context
            );
            assert.equal('withCredentials', methodName);
            assert.deepEqual(
              { username: 'user1',
                password: 'pass1' },
              params
            );
            
            done();
          } catch (e) {
            done(e);
          }
          
          return new Promise(function () {
          });
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'TokenProvider.StubTokenProvider',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'get-token',
          {
            fakeToken: true
          }, // stack token
          {
            someContext: 'someValue'
          }, // context
          'withCredentials', // method name
          { username: 'user1',
            password: 'pass1' }, // params
          function () {
          }
        );
      });
    });
    
    it('returns error on promise reject', function (done) {
      var target = new TokenProviderInitializer({
        getName: function () { return 'StubTokenProvider'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          return Promise.reject('some error');
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'TokenProvider.StubTokenProvider',
        {}
      );
      
      fakeSocket
      .emit(
        'get-token',
        {
        }, // stack token
        {
        }, // context
        'withCredentials', // method name
        { username: 'user1',
            password: 'pass1' }, // params
        function (err) {
          try {
            assert.equal('some error', err);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
    
    it('returns result on promise resolution', function (done) {
      var target = new TokenProviderInitializer({
        getName: function () { return 'StubTokenProvider'; },
        invokeMethod: function (stackToken, context, methodName, params) {
          return Promise.resolve({ 'username': 'user1' });
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'TokenProvider.StubTokenProvider',
        {}
      );
      
      fakeSocket
      .emit(
        'get-token',
        {
        }, // stack token
        {
        }, // context
        'withCredentials', // method name
        { 'username': 'user1',
          'password': 'pass1' }, // params
        function (err, result) {
          try {
            assert(!err);
            assert.deepEqual({ 'username': 'user1' }, result);
            
            done();
          } catch (e) {
            done(e);
          }
        }
      );
    });
  });
  
  describe('context client parameter', function () {
    it('requires a second socket for invoking dependencies', function (done) {
      var target = new TokenProviderInitializer(
        {
          getName: function () { return 'StubTokenProvider'; },
          invokeMethod: function (stackToken, context, methodName, params) {
            return Promise.resolve('my result');
          }
        },
        {
          'dep1': 'Dep1'
        }
      );
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'TokenProvider.StubTokenProvider',
        {
          createSocket: function (namespace, initializer) {
            try {
              assert.equal('', namespace);
              assert.equal('function', typeof(initializer));
              
              done();
            } catch (e) {
              done(e);
            }
          }
        }
      );
    });
  });
  
  describe('#get-methods event', function () {
    it('returns methods', function (done) {
      var target = new TokenProviderInitializer({
        getName: function () { return 'StubTokenProvider'; },
        describeMethods: function () {
          return {
            'withCredentials': [ 'username', 'password' ]
          };
        }
      });
      var fakeSocket = new EventEmitter();
      
      target(
        fakeSocket,
        'TokenProvider.StubTokenProvider',
        {}
      )
      .then(function () {
        fakeSocket
        .emit(
          'get-methods',
          function (err, methods) {
            try {
              assert(!err);
              assert.deepEqual(
                {
                  'withCredentials': [ 'username', 'password' ]
                },
                methods
              );
              
              done();
            } catch (e) {
              done(e);
            }
          }
        );
      });
    });
  });
});